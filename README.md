# Mark2

Another template engine with mustache syntax written in pure javascript.

Why another one? It's faster then "Handlebars", and has more features then "Hogan".

I was looking for suitable implementation of mustache renderer in javascript.
I found github place of Mark Adam and his Markup.
It was renderer, but the syntax did not comply Mustache,
he invented his own variant of syntax, but very similar to. So I decided to fork this project.

I ended up with complete rework, so this code has not much common with original Markup,
except of built-in Pipes (filters), which are almost completely borrowed from Mark.

About the name: as it is known from the bio of known writter, Mark Twain means "Mark number two".
And, also, Samuel's Langhorne Clemens (Mark Twain's) mustaches was very impressive.
<a name="basic-usage"></a>
## Basic Usage

At first, include this "Mark2.js" into the HTML.
```html
<script type="text/javascript" src="/resources/js/mark2.js"></script>
```

And then, you can utilize it in code.
You can either compile template before usage or render directly from template.
In case of compiled template, you can store "compiled" somewhere. In local storage,
or on server, and download with ajax. It could spare some time and resources.

The last row in following example is jQuery based, but jQuery is not required for Mark2.js

example:
```javascript
/** definitions:  **/
var data = {
  'any':{
    'json':['tree', 'of', 'data'],
    'as':['you', 'wish']
  }
};
var tplText = '<div>{{any.json.2}}</div>';
/** application:  **/
var compiled = Mark.compile(tplText);
var prepared = Mark.render(compiled, data);
$('body').html(prepared);
```
example:
```javascript
var data = {
  'any':{
    'json':['tree', 'of', 'data'],
    'as':['you', 'wish']
  }
};
var tplText = '<div>{{any.json.2}}</div>';

var prepared = Mark.render(tplText, data);
$('body').html(prepared);
```

<a name="features"></a>
## Features

<a name="dot-notation"></a>
### Dot Notation

example:
```html
{{blue.1.data.0.title}}
```
This extends Mustache syntax, and allows to define multiple steps into the branches in one variable.
Index of variables in lists could be also addressed.
In original Mustache, when it comes to (not associative) array, we must process all or nothing.
It is zero based.
So in the example above, we are
accessing second (1) element of array "blue" and then, inside, first (0) element of array "data".
Then accessed element is apparently object, and contains element with key "title".

<a name="root-bounce"></a>
### Root Bounce

example:
```html
{{:blue.1.data.0.title}}
```
example:
```html
{{#:black}}{{#magic}}{{woman}}{{/magic}}{{/:black}}
```
This extends Mustache syntax, and allows us to jump back to root of the dataset tree,
while we are deep inside another loops of tree.

<a name="negation"></a>
### Negation

example:
```html
{{^data}} Have no data! {{/data}}
```
This is part of Mustache syntax. Given part of template is displayed only when variable data is undefined, or it is boolean false.

<a name="builtin-variables"></a>
### Built-in Variables

example:
```html
{{@path}}
```
There are some variables around, used for rendering. So why not to make them available.

Variable | Shortcut | Description
--- | --- | ---
@index | $ | Returns numerical index of given iteration in lists. Zero based.
@idx | $$ | Returns numerical index of given iteration in lists. One based.
@length | | Returns count of items in given list.
@path | | Returns full path in dot-notation.
@time | | Returns time of rendering.

<a name="partials"></a>
### Support For Partials

example:
```html
 {{#!register partial1}}
   <h3>{{titlee}}</h3>
   <h4>Thats the partial!</h4>
   {{#fun}}
     {{place|upcase}}
     And the beat goes on!
   {{/fun}}
 {{/!register}}
```
And then:
```html
{{>partial1}}
```
Partials are known concept of Mustache, altough not all renderers support it.
If some part of code repeats on multiple places, we can define it only once,
and then introduce only the name of such part, which is extended to full code again.
MarkTwain allows register partial through the API, and also directly inside the template.


<a name="pipes"></a>
### Pipes (or Filters)

example:
```json
{sisters:['Anikó', 'Tünde', 'Emöke']}
```
```html
{{sisters|join> & }}
```
Result: Anikó & Tünde & Emöke

This concept is present in nearly all renderers.
It allows to use filter, to make simple operations on dataset nodes.
We can utilize built-in Pipes, or define our own through API.
Please note, that character > is used as delimiter for pipe's variables.

<a name="conditionals"></a>
### Conditional Statements

example:
```html
{{if age|more>75}} {{if gender|equals>male}} Old man! {{else}} Old woman! {{/if}} {{/if}}
```
This idea is borrowed from Mark. Nice one.
It allows us to build conditional statements with the help of Pipes (filters).


## API

method | variables | description
--- | --- | ---
compile | template | Prepares compiled template: in other words, converts html to javascript. Returns javascript as string.
render | template, dataset | Mixing directly template with json dataset. Returns string.
render | compiled, dataset | Mixing compiled template with json dataset. Returns string.
registerHelper | name, helperFunction | Registers helper function.
unregisterHelper | name | Unregisters helper function.
registerGlobal | name, content | Registers global variable.
unregisterGlobal | name | Unregisters global variable.
registerPartial | name, content | Registers partial template.
unregisterPartial | name | Unregisters partial template.

<a name="list-of-builtin-pipes"></a>
## List Of Built-in Pipes
Word "variable" in this scope means first word in mustaches.
In example
```html
{{sisters|join> & }}
```
"sisters" is "VARIABLE", word "join" is "PIPE" and character "&" is "PARAMETER".

pipe | parameters | description
--- | --- | ---
empty | | Returns variable only if it is empty, else returns boolean "false".
notempty | | Returns variable only if it is NOT empty, else returns boolean "false".
blank | | -
more | >n | Returns variable if it is array or string, with more elements (characters) then n. Returns variable if it is number, greater then n. In other cases returns false.
less | >n | Returns variable if it is array or string, with less elements (characters) then n. Returns variable if it is number, smaller then n. In other cases returns false.
ormore | >n | Returns variable if it is array or string, with more or equal elements (characters) then n. Returns variable if it is number, greater or equal to n. In other cases returns false.
orless | >n | Returns variable if it is array or string, with less or equal elements (characters) then n. Returns variable if it is number, smaller or equal to n. In other cases returns false.
between | >n1 >n2 | Returns variable if it is array or string, with count of elements (characters) between n1 and n2. Returns variable if it is number, between n1 and n2. In other cases returns false.
equals | >x | Returns variable, if equals to "x", else returns boolean "false"; 
notequals | >x | Returns variable, if doesnt equals to "x", else returns boolean "false";
like | >x | Returns variable, if matches regexp "x", else returns boolean "false";
notlike | >x | Returns variable, if doesnt match regexp "x", else returns boolean "false";
upcase | | If variable is string, it is returned uppercased.
downcase | | If variable is string, it is returned lowercased.
capcase | | If variable is string, it is returned with all first characters uppercased.
chop | >n | If variable is string, it is spliced to characters, and then only "n" characters followed by "..." is returned.
tease | >n | If variable is string, it is spliced to words, and then only "n" words followed by "..." is returned.
trim | | If variable is string, it is returned trimmed.
pack | | If variable is string, it is returned trimmed, and bigger spaces then one are returned as exactly one space character.
round | | If variable is number, it is returned rounded to integer.
clean | | If variable is string, and contains tags, tags are removed. 
size | | If variable is array or string, its length is returned.
length | | If variable is array or string, its length is returned.
reverse | | If variable is array, its returned reversed.
join | >glue | If variable is array of strings, it is glued together with "glue". String is returned.
limit | >count >offset | If variable is array, only "count" elements, starting with "offset", is returned. Offset defaults to zero.
split | >separator | If variable is string, it is splitted by separator and array is returned. Separator defaults to ",".
choose | >yes >no | If variable is boolean and is true, "yes" parameter is returned, else "no" parameter is returned.
toggle | | 
sort | | If variable is array, it is returned sorted. 
fix | >decimals | Turns real number to string representation with fixed decimals.
mod | >divisor | If variable is number, it is returned modulo divisor.
divisible | >divisor | If variable is number and is divisible by "divisor", returns number. Else returns "false".
even | | If variable is number and is even, returns number. Else returns "false".
odd | | If variable is number and is odd, returns number. Else returns "false".
number | | If variable is number, returns number. Else returns "false".
url | | If variable is string, it is returned urlencoded.
bool | | If variable is boolean false, number 0, or empty string, or undefined, boolean "false" is returned, else boolean "true" is returned.
falsy | | If variable is boolean false, number 0, or empty string, or undefined, boolean "true" is returned, else boolean "false" is returned.
first | | Returns first element of array.
last | | Returns last element of array.
nth | >n | Returns ">n" nth element of array. ">n" is one based.
call | >fn >other params... | If variable contains function ">fn", then function is called with the rest of parameters. 
set | >key | This will set the variable's value as global variable with key ">key" 
log | | Invokes console.log. Good for debugging.
path | | Returns variable's path in data tree. 
json | | Returns variable as JSON string.

